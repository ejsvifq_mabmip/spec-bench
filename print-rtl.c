/* Print RTL for GCC.
   Copyright (C) 1987, 1988, 1992, 1997, 1998, 1999, 2000, 2002, 2003,
   2004, 2005, 2007, 2008, 2009, 2010
   Free Software Foundation, Inc.

This file is part of GCC.

GCC is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free
Software Foundation; either version 3, or (at your option) any later
version.

GCC is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with GCC; see the file COPYING3.  If not see
<http://www.gnu.org/licenses/>.  */

/* This file is compiled twice: once for the generator programs,
   once for the compiler.  */
#ifdef GENERATOR_FILE
#include "bconfig.h"
#else
#include "config.h"
#endif

#include "system.h"
#include "coretypes.h"
#include "tm.h"
#include "rtl.h"

/* These headers all define things which are not available in
   generator programs.  */
#ifndef GENERATOR_FILE
#include "tree.h"
#include "real.h"
#include "flags.h"
#include "hard-reg-set.h"
#include "basic-block.h"
#include "diagnostic.h"
#include "cselib.h"
#include "tree-pass.h"
#endif
#include<fast_io_legacy.h>
//static FILE *outfile;

static c_io_observer_unlocked outfile;

static int sawclose = 0;

static int indent;

static void print_rtx (const_rtx);

/* String printed at beginning of each RTL when it is dumped.
   This string is set to ASM_COMMENT_START when the RTL is dumped in
   the assembly output file.  */
const char *print_rtx_head = "";

/* Nonzero means suppress output of instruction numbers
   in debugging dumps.
   This must be defined here so that programs like gencodes can be linked.  */
int flag_dump_unnumbered = 0;

/* Nonzero means suppress output of instruction numbers for previous
   and next insns in debugging dumps.
   This must be defined here so that programs like gencodes can be linked.  */
int flag_dump_unnumbered_links = 0;

/* Nonzero means use simplified format without flags, modes, etc.  */
int flag_simple = 0;

/* Nonzero if we are dumping graphical description.  */
int dump_for_graph;

#ifndef GENERATOR_FILE
void
print_mem_expr (FILE *outfile, const_tree expr)
{
	c_io_lock_guard ciog(outfile);
	c_io_observer_unlocked ciob(outfile);
	put(ciob,' ');
//  fputc (' ', outfile);
  print_generic_expr (outfile, CONST_CAST_TREE (expr), dump_flags);
}
#endif

/* Print IN_RTX onto OUTFILE.  This is the recursive part of printing.  */

static void
print_rtx (const_rtx in_rtx)
{
  int i = 0;
  int j;
  const char *format_ptr;
  int is_insn;

  if (sawclose)
    {
      if (flag_simple)
	  	put(outfile,' ');
//	fputc (' ', outfile);
      else
//	  	print(outfile,);
	fprintf_unlocked (outfile.native_handle(), "\n%s%*s", print_rtx_head, indent * 2, "");
      sawclose = 0;
    }

  if (in_rtx == 0)
    {
		put(outfile,"(nil)");
//      fputs ("(nil)", outfile);
      sawclose = 1;
      return;
    }
  else if (GET_CODE (in_rtx) > NUM_RTX_CODE)
    {
       fprintf_unlocked (outfile.native_handle(), "(??? bad code %d\n%s%*s)", GET_CODE (in_rtx),
		print_rtx_head, indent * 2, "");
       sawclose = 1;
       return;
    }

  is_insn = INSN_P (in_rtx);

  /* When printing in VCG format we write INSNs, NOTE, LABEL, and BARRIER
     in separate nodes and therefore have to handle them special here.  */
  if (dump_for_graph
      && (is_insn || NOTE_P (in_rtx)
	  || LABEL_P (in_rtx) || BARRIER_P (in_rtx)))
    {
      i = 3;
      indent = 0;
    }
  else
    {
      /* Print name of expression code.  */
      if (flag_simple && CONST_INT_P (in_rtx))
	  	put(outfile,'(');
//	fputc ('(', outfile);
      else
	  	print(outfile,"(",GET_RTX_NAME (GET_CODE (in_rtx)));
//	fprintf (outfile, "(%s", GET_RTX_NAME (GET_CODE (in_rtx)));

      if (! flag_simple)
	{
	  if (RTX_FLAG (in_rtx, in_struct))
	  	print(outfile,"/s");
//	    fputs ("/s", outfile);

	  if (RTX_FLAG (in_rtx, volatil))
	  	print(outfile,"/v");
//	    fputs ("/v", outfile);

	  if (RTX_FLAG (in_rtx, unchanging))
	  	print(outfile,"/u");
//	    fputs ("/u", outfile);

	  if (RTX_FLAG (in_rtx, frame_related))
	  	print(outfile,"/f");
//	    fputs ("/f", outfile);

	  if (RTX_FLAG (in_rtx, jump))
		  print(outfile,"/j");
//	    fputs ("/j", outfile);

	  if (RTX_FLAG (in_rtx, call))
		  print(outfile,"/c");
//	    fputs ("/c", outfile);

	  if (RTX_FLAG (in_rtx, return_val))
		  print(outfile,"/i");
//	    fputs ("/i", outfile);

	  /* Print REG_NOTE names for EXPR_LIST and INSN_LIST.  */
	  if ((GET_CODE (in_rtx) == EXPR_LIST
	       || GET_CODE (in_rtx) == INSN_LIST)
	      && (int)GET_MODE (in_rtx) < REG_NOTE_MAX)
//	    fprintf (outfile, ":%s",
		print (outfile, ":",
		     GET_REG_NOTE_NAME (GET_MODE (in_rtx)));

	  /* For other rtl, print the mode if it's not VOID.  */
	  else if (GET_MODE (in_rtx) != VOIDmode)
//	    fprintf (outfile, ":%s",
		print (outfile, ":", GET_MODE_NAME (GET_MODE (in_rtx)));

#ifndef GENERATOR_FILE
	  if (GET_CODE (in_rtx) == VAR_LOCATION)
	    {
	      if (TREE_CODE (PAT_VAR_LOCATION_DECL (in_rtx)) == STRING_CST)
			print(outfile," <debug string placeholder>");
//		fputs (" <debug string placeholder>", outfile);
	      else
		print_mem_expr (outfile.native_handle(), PAT_VAR_LOCATION_DECL (in_rtx));
			put(outfile,' ');
//	      fputc (' ', outfile);
	      print_rtx (PAT_VAR_LOCATION_LOC (in_rtx));
	      if (PAT_VAR_LOCATION_STATUS (in_rtx)
		  == VAR_INIT_STATUS_UNINITIALIZED)
//		fprintf (outfile, " [uninit]");
		print(outfile," [uninit]");
	      sawclose = 1;
	      i = GET_RTX_LENGTH (VAR_LOCATION);
	    }
#endif
	}
    }

#ifndef GENERATOR_FILE
  if (GET_CODE (in_rtx) == CONST_DOUBLE && FLOAT_MODE_P (GET_MODE (in_rtx)))
    i = 5;
#endif

  /* Get the format string and skip the first elements if we have handled
     them already.  */
  format_ptr = GET_RTX_FORMAT (GET_CODE (in_rtx)) + i;
  for (; i < GET_RTX_LENGTH (GET_CODE (in_rtx)); i++)
    switch (*format_ptr++)
      {
	const char *str;

      case 'T':
	str = XTMPL (in_rtx, i);
	goto string;

      case 'S':
      case 's':
	str = XSTR (in_rtx, i);
      string:

	if (str == 0)
		print(outfile,dump_for_graph ? " \\\"\\\"" : " \"\"");
//	  fputs (dump_for_graph ? " \\\"\\\"" : " \"\"", outfile);
	else
	  {
	    if (dump_for_graph)
//	      fprintf (outfile, " (\\\"%s\\\")", str);
			print(outfile," (\\\"",str,"\\\")");
	    else
//	      fprintf (outfile, " (\"%s\")", str);
	  }
	sawclose = 1;
	break;

	/* 0 indicates a field for internal use that should not be printed.
	   An exception is the third field of a NOTE, where it indicates
	   that the field has several different valid contents.  */
      case '0':
	if (i == 1 && REG_P (in_rtx))
	  {
	    if (REGNO (in_rtx) != ORIGINAL_REGNO (in_rtx))
//	      fprintf (outfile, " [%d]", ORIGINAL_REGNO (in_rtx));
	      print (outfile, " [", ORIGINAL_REGNO (in_rtx),"]");
	  }
#ifndef GENERATOR_FILE
	else if (i == 1 && GET_CODE (in_rtx) == SYMBOL_REF)
	  {
	    int flags = SYMBOL_REF_FLAGS (in_rtx);
	    if (flags)
			print(outfile," [flags 0x",fast_io::hex(flags),"]");
//	      fprintf (outfile, " [flags 0x%x]", flags);
	  }
	else if (i == 2 && GET_CODE (in_rtx) == SYMBOL_REF)
	  {
	    tree decl = SYMBOL_REF_DECL (in_rtx);
	    if (decl)
	      print_node_brief (outfile.native_handle(), "", decl, dump_flags);
	  }
#endif
	else if (i == 4 && NOTE_P (in_rtx))
	  {
	    switch (NOTE_KIND (in_rtx))
	      {
	      case NOTE_INSN_EH_REGION_BEG:
	      case NOTE_INSN_EH_REGION_END:
		if (flag_dump_unnumbered)
			print(outfile," #");
//		  fprintf (outfile, " #");
		else
			print(outfile," ",NOTE_EH_HANDLER (in_rtx));
//		  fprintf (outfile, " %d", NOTE_EH_HANDLER (in_rtx));
		sawclose = 1;
		break;

	      case NOTE_INSN_BLOCK_BEG:
	      case NOTE_INSN_BLOCK_END:
#ifndef GENERATOR_FILE
		dump_addr (outfile.native_handle(), " ", NOTE_BLOCK (in_rtx));
#endif
		sawclose = 1;
		break;

	      case NOTE_INSN_BASIC_BLOCK:
		{
#ifndef GENERATOR_FILE
		  basic_block bb = NOTE_BASIC_BLOCK (in_rtx);
		  if (bb != 0)
		  	print(outfile," [bb ",bb->index,"]");
//		    fprintf (outfile, " [bb %d]", bb->index);
#endif
		  break;
	        }

	      case NOTE_INSN_DELETED_LABEL:
		{
		  const char *label = NOTE_DELETED_LABEL_NAME (in_rtx);
		  if (label)
		  	print(outfile," (\"",label,"\")");
//		    fprintf (outfile, " (\"%s\")", label);
		  else
		  	print(outfile," \"\"");
//		    fprintf (outfile, " \"\"");
		}
		break;

	      case NOTE_INSN_SWITCH_TEXT_SECTIONS:
		{
#ifndef GENERATOR_FILE
		  basic_block bb = NOTE_BASIC_BLOCK (in_rtx);
		  if (bb != 0)
		  	print(outfile," [bb ",bb->index,"]");
//		    fprintf (outfile, " [bb %d]", bb->index);
#endif
		  break;
		}

	      case NOTE_INSN_VAR_LOCATION:
#ifndef GENERATOR_FILE
			put(outfile,' ');
//		fputc (' ', outfile);
		print_rtx (NOTE_VAR_LOCATION (in_rtx));
#endif
		break;

	      default:
		break;
	      }
	  }
	else if (i == 8 && JUMP_P (in_rtx) && JUMP_LABEL (in_rtx) != NULL)
	  /* Output the JUMP_LABEL reference.  */
	  fprintf (outfile.native_handle(), "\n%s%*s -> %d", print_rtx_head, indent * 2, "",
		   INSN_UID (JUMP_LABEL (in_rtx)));
	else if (i == 0 && GET_CODE (in_rtx) == VALUE)
	  {
#ifndef GENERATOR_FILE
	    cselib_val *val = CSELIB_VAL_PTR (in_rtx);

//	    fprintf (outfile, " %u:%u", val->uid, val->hash);
		print(outfile," ",val->uid,":",val->hash);
	    dump_addr (outfile.native_handle(), " @", in_rtx);
	    dump_addr (outfile.native_handle(), "/", (void*)val);
#endif
	  }
	else if (i == 0 && GET_CODE (in_rtx) == DEBUG_EXPR)
	  {
#ifndef GENERATOR_FILE
//	    fprintf (outfile, " D#%i",
		print(outfile.native_handle()," D#",
		     DEBUG_TEMP_UID (DEBUG_EXPR_TREE_DECL (in_rtx)));
#endif
	  }
	break;

      case 'e':
      do_e:
	indent += 2;
	if (!sawclose)
	  fprintf (outfile, " ");
	print_rtx (XEXP (in_rtx, i));
	indent -= 2;
	break;

      case 'E':
      case 'V':
	indent += 2;
	if (sawclose)
	  {
	    fprintf (outfile.native_handle(), "\n%s%*s",
		     print_rtx_head, indent * 2, "");
	    sawclose = 0;
	  }
	  print(outfile," [");
//	fputs (" [", outfile);
	if (NULL != XVEC (in_rtx, i))
	  {
	    indent += 2;
	    if (XVECLEN (in_rtx, i))
	      sawclose = 1;

	    for (j = 0; j < XVECLEN (in_rtx, i); j++)
	      print_rtx (XVECEXP (in_rtx, i, j));

	    indent -= 2;
	  }
	if (sawclose)
	  fprintf (outfile.native_handle(), "\n%s%*s", print_rtx_head, indent * 2, "");
	print(outfile,"]");
//	fputs ("]", outfile);
	sawclose = 1;
	indent -= 2;
	break;

      case 'w':
	if (! flag_simple)
		print(outfile," ", XWINT (in_rtx, i));
//	  fprintf (outfile, " ");
//	fprintf (outfile, HOST_WIDE_INT_PRINT_DEC, XWINT (in_rtx, i));
	if (! flag_simple)
		print(outfile," [0x",(unsigned HOST_WIDE_INT) XWINT (in_rtx, i),"]");
//	  fprintf (outfile, " [" HOST_WIDE_INT_PRINT_HEX "]",
//		   (unsigned HOST_WIDE_INT) XWINT (in_rtx, i));
	break;

      case 'i':
	if (i == 4 && INSN_P (in_rtx))
	  {
#ifndef GENERATOR_FILE
	    /*  Pretty-print insn locators.  Ignore scoping as it is mostly
		redundant with line number information and do not print anything
		when there is no location information available.  */
	    if (INSN_LOCATOR (in_rtx) && insn_file (in_rtx))
			print(outfile," ",insn_file (in_rtx),":",insn_line (in_rtx));
//	      fprintf(outfile, " %s:%i", insn_file (in_rtx), insn_line (in_rtx));
#endif
	  }
	else if (i == 6 && GET_CODE (in_rtx) == ASM_OPERANDS)
	  {
#ifndef GENERATOR_FILE
			print(outfile," ",locator_file (ASM_OPERANDS_SOURCE_LOCATION (in_rtx)),":",locator_line (ASM_OPERANDS_SOURCE_LOCATION (in_rtx)));
//	    fprintf (outfile, " %s:%i",
//		     locator_file (ASM_OPERANDS_SOURCE_LOCATION (in_rtx)),
//		     locator_line (ASM_OPERANDS_SOURCE_LOCATION (in_rtx)));
#endif
	  }
	else if (i == 1 && GET_CODE (in_rtx) == ASM_INPUT)
	  {
#ifndef GENERATOR_FILE
			print(outfile," ",locator_file (ASM_OPERANDS_SOURCE_LOCATION (in_rtx)),":",locator_line (ASM_OPERANDS_SOURCE_LOCATION (in_rtx)));
//	    fprintf (outfile, " %s:%i",
//		     locator_file (ASM_INPUT_SOURCE_LOCATION (in_rtx)),
//		     locator_line (ASM_INPUT_SOURCE_LOCATION (in_rtx)));
#endif
	  }
	else if (i == 6 && NOTE_P (in_rtx))
	  {
	    /* This field is only used for NOTE_INSN_DELETED_LABEL, and
	       other times often contains garbage from INSN->NOTE death.  */
	    if (NOTE_KIND (in_rtx) == NOTE_INSN_DELETED_LABEL)
			print(outfile," ",XINT (in_rtx, i));
//	      fprintf (outfile, " %d",  XINT (in_rtx, i));
	  }
	else
	  {
	    int value = XINT (in_rtx, i);
	    const char *name;

#ifndef GENERATOR_FILE
	    if (REG_P (in_rtx) && value < FIRST_PSEUDO_REGISTER)
//	      fprintf (outfile, " %d %s", REGNO (in_rtx),
//		       reg_names[REGNO (in_rtx)]);
			print(outfile," ",REGNO (in_rtx)," ",reg_names[REGNO (in_rtx)]);
	    else if (REG_P (in_rtx)
		     && value <= LAST_VIRTUAL_REGISTER)
	      {
			  print(outfile," ",value);
		if (value == VIRTUAL_INCOMING_ARGS_REGNUM)
			print(outfile," virtual-incoming-args");
//		  fprintf (outfile, " %d virtual-incoming-args", value);
		else if (value == VIRTUAL_STACK_VARS_REGNUM)
			print(outfile," virtual-stack-vars");
//		  fprintf (outfile, " %d virtual-stack-vars", value);
		else if (value == VIRTUAL_STACK_DYNAMIC_REGNUM)
			print(outfile," virtual-stack-dynamic");
//		  fprintf (outfile, " %d virtual-stack-dynamic", value);
		else if (value == VIRTUAL_OUTGOING_ARGS_REGNUM)
			print(outfile," virtual-outgoing-args");
//		  fprintf (outfile, " %d virtual-outgoing-args", value);
		else if (value == VIRTUAL_CFA_REGNUM)
			print(outfile," virtual-cfa");
//		  fprintf (outfile, " %d virtual-cfa", value);
		else
			print(outfile," virtual-reg-",value-FIRST_VIRTUAL_REGISTER);
//		  fprintf (outfile, " %d virtual-reg-%d", value,
//			   value-FIRST_VIRTUAL_REGISTER);
	      }
	    else
#endif
	      if (flag_dump_unnumbered
		     && (is_insn || NOTE_P (in_rtx)))
			put(outfile,'#');
//	      fputc ('#', outfile);
	    else
			print(outfile," ",value);
//	      fprintf (outfile, " %d", value);

#ifndef GENERATOR_FILE
	    if (REG_P (in_rtx) && REG_ATTRS (in_rtx))
	      {
			  print(outfile," [",value);
//		fputs (" [", outfile);
		if (ORIGINAL_REGNO (in_rtx) != REGNO (in_rtx))
			  print(outfile,"orig:",ORIGINAL_REGNO (in_rtx));
//		  fprintf (outfile, "orig:%i", ORIGINAL_REGNO (in_rtx));
		if (REG_EXPR (in_rtx))
		  print_mem_expr (outfile.native_handle(), REG_EXPR (in_rtx));

		if (REG_OFFSET (in_rtx))
			print(outfile,"+",REG_OFFSET (in_rtx));
//		  fprintf (outfile, "+" HOST_WIDE_INT_PRINT_DEC,
//			   REG_OFFSET (in_rtx));
//		fputs (" ]", outfile);
			put(outfile," ]");
	      }
#endif

	    if (is_insn && &INSN_CODE (in_rtx) == &XINT (in_rtx, i)
		&& XINT (in_rtx, i) >= 0
		&& (name = get_insn_name (XINT (in_rtx, i))) != NULL)
			print(outfile," {",name,"}");
//	      fprintf (outfile, " {%s}", name);
	    sawclose = 0;
	  }
	break;

      /* Print NOTE_INSN names rather than integer codes.  */

      case 'n':
	  	print(outfile," ",GET_NOTE_INSN_NAME (XINT (in_rtx, i)));
//	fprintf (outfile, " %s", GET_NOTE_INSN_NAME (XINT (in_rtx, i)));
	sawclose = 0;
	break;

      case 'u':
	if (XEXP (in_rtx, i) != NULL)
	  {
	    rtx sub = XEXP (in_rtx, i);
	    enum rtx_code subc = GET_CODE (sub);

	    if (GET_CODE (in_rtx) == LABEL_REF)
	      {
		if (subc == NOTE
		    && NOTE_KIND (sub) == NOTE_INSN_DELETED_LABEL)
		  {
		    if (flag_dump_unnumbered)
				print(outfile," [# deleted]");
//		      fprintf (outfile, " [# deleted]");
		    else
				print(outfile," [",INSN_UID (sub)," deleted]");
//		      fprintf (outfile, " [%d deleted]", INSN_UID (sub));
		    sawclose = 0;
		    break;
		  }

		if (subc != CODE_LABEL)
		  goto do_e;
	      }

	    if (flag_dump_unnumbered
		|| (flag_dump_unnumbered_links && (i == 1 || i == 2)
		    && (INSN_P (in_rtx) || NOTE_P (in_rtx)
			|| LABEL_P (in_rtx) || BARRIER_P (in_rtx))))
			print(outfile," #");
//	      fputs (" #", outfile);
	    else
//	      fprintf (outfile, " %d", INSN_UID (sub));
			print(outfile," ", INSN_UID (sub));
	  }
	else
		print(outfile," 0");
//	  fputs (" 0", outfile);
	sawclose = 0;
	break;

      case 'b':
#ifndef GENERATOR_FILE
	if (XBITMAP (in_rtx, i) == NULL)
		print(outfile," {null}");
//	  fputs (" {null}", outfile);
	else
	  bitmap_print (outfile.native_handle(), XBITMAP (in_rtx, i), " {", "}");
#endif
	sawclose = 0;
	break;

      case 't':
#ifndef GENERATOR_FILE
	dump_addr (outfile.native_handle(), " ", XTREE (in_rtx, i));
#endif
	break;

      case '*':
		print(outfile," Unknown");
//	fputs (" Unknown", outfile);
	sawclose = 0;
	break;

      case 'B':
#ifndef GENERATOR_FILE
	if (XBBDEF (in_rtx, i))
		print(outfile," ", XBBDEF (in_rtx, i)->index);
//	  fprintf (outfile, " %i", XBBDEF (in_rtx, i)->index);
#endif
	break;

      default:
	gcc_unreachable ();
      }

  switch (GET_CODE (in_rtx))
    {
#ifndef GENERATOR_FILE
    case MEM:
      if (__builtin_expect (final_insns_dump_p, false))
	  	print(outfile," [");
//	fprintf (outfile, " [");
      else
//	fprintf (outfile, " [" HOST_WIDE_INT_PRINT_DEC,
//		 (HOST_WIDE_INT) MEM_ALIAS_SET (in_rtx));
		print(outfile," [",(HOST_WIDE_INT) MEM_ALIAS_SET (in_rtx));
      if (MEM_EXPR (in_rtx))
	print_mem_expr (outfile.native_handle(), MEM_EXPR (in_rtx));

      if (MEM_OFFSET (in_rtx))
//	fprintf (outfile, "+" HOST_WIDE_INT_PRINT_DEC,
	print(outfile,"+",
		 INTVAL (MEM_OFFSET (in_rtx)));

      if (MEM_SIZE (in_rtx))
//	fprintf (outfile, " S" HOST_WIDE_INT_PRINT_DEC,
	print(outfile," S",
		 INTVAL (MEM_SIZE (in_rtx)));

      if (MEM_ALIGN (in_rtx) != 1)
//	fprintf (outfile, " A%u", MEM_ALIGN (in_rtx));
	print(outfile," A", MEM_ALIGN (in_rtx));
      if (!ADDR_SPACE_GENERIC_P (MEM_ADDR_SPACE (in_rtx)))
//	fprintf (outfile, " AS%u", MEM_ADDR_SPACE (in_rtx));
	print(outfile," AS",MEM_ADDR_SPACE (in_rtx))
//      fputc (']', outfile);
	put(outfile,']');
      break;

    case CONST_DOUBLE:
      if (FLOAT_MODE_P (GET_MODE (in_rtx)))
	{
	  char s[60];

	  real_to_decimal (s, CONST_DOUBLE_REAL_VALUE (in_rtx),
			   sizeof (s), 0, 1);
//	  fprintf (outfile, " %s", s);
		print(outfile," ",s);
	  real_to_hexadecimal (s, CONST_DOUBLE_REAL_VALUE (in_rtx),
			       sizeof (s), 0, 1);
//	  fprintf (outfile, " [%s]", s);
		print(outfile," [",s,"]");
	}
      break;
#endif

    case CODE_LABEL:
//      fprintf (outfile, " [%d uses]", LABEL_NUSES (in_rtx));
		print(outfile," [", LABEL_NUSES (in_rtx)," uses]");
      switch (LABEL_KIND (in_rtx))
	{
	  case LABEL_NORMAL: break;
	  case LABEL_STATIC_ENTRY:
//	   fputs (" [entry]", outfile);
		print(outfile," [entry]");
	  break;
	  case LABEL_GLOBAL_ENTRY:
//	  fputs (" [global entry]", outfile);
		print(outfile," [global entry]");
	  break;
	  case LABEL_WEAK_ENTRY:
//	  fputs (" [weak entry]", outfile);
		print(outfile," [weak entry]");
	  break;
	  default: gcc_unreachable ();
	}
      break;

    default:
      break;
    }

  if (dump_for_graph
      && (is_insn || NOTE_P (in_rtx)
	  || LABEL_P (in_rtx) || BARRIER_P (in_rtx)))
    sawclose = 0;
  else
    {
//      fputc (')', outfile);
		put(outfile,')');
      sawclose = 1;
    }
}

/* Print an rtx on the current line of FILE.  Initially indent IND
   characters.  */

void
print_inline_rtx (FILE *outf, const_rtx x, int ind)
{
	c_io_lock_guard lg(outf);
  int oldsaw = sawclose;
  int oldindent = indent;

  sawclose = 0;
  indent = ind;
  outfile.fp = outf;
  print_rtx (x);
  sawclose = oldsaw;
  indent = oldindent;
}

/* Call this function from the debugger to see what X looks like.  */

void
debug_rtx (const_rtx x)
{
	c_io_lock_guard lg{stderr};
  outfile.fp = stderr;
  sawclose = 0;
  print_rtx (x);
//  fprintf (stderr, "\n");
	put(outfile,'\n');
}

/* Count of rtx's to print with debug_rtx_list.
   This global exists because gdb user defined commands have no arguments.  */

int debug_rtx_count = 0;	/* 0 is treated as equivalent to 1 */

/* Call this function to print list from X on.

   N is a count of the rtx's to print. Positive values print from the specified
   rtx on.  Negative values print a window around the rtx.
   EG: -5 prints 2 rtx's on either side (in addition to the specified rtx).  */

void
debug_rtx_list (const_rtx x, int n)
{
  int i,count;
  const_rtx insn;

  count = n == 0 ? 1 : n < 0 ? -n : n;

  /* If we are printing a window, back up to the start.  */

  if (n < 0)
    for (i = count / 2; i > 0; i--)
      {
	if (PREV_INSN (x) == 0)
	  break;
	x = PREV_INSN (x);
      }

  for (i = count, insn = x; i > 0 && insn != 0; i--, insn = NEXT_INSN (insn))
    {
      debug_rtx (insn);
//      fprintf (stderr, "\n");
		print(fast_io::c_stderr,"\n");
    }
}

/* Call this function to print an rtx list from START to END inclusive.  */

void
debug_rtx_range (const_rtx start, const_rtx end)
{
  while (1)
    {
      debug_rtx (start);
//      fprintf (stderr, "\n");
		print(fast_io::c_stderr,"\n");
      if (!start || start == end)
	break;
      start = NEXT_INSN (start);
    }
}

/* Call this function to search an rtx list to find one with insn uid UID,
   and then call debug_rtx_list to print it, using DEBUG_RTX_COUNT.
   The found insn is returned to enable further debugging analysis.  */

const_rtx
debug_rtx_find (const_rtx x, int uid)
{
  while (x != 0 && INSN_UID (x) != uid)
    x = NEXT_INSN (x);
  if (x != 0)
    {
      debug_rtx_list (x, debug_rtx_count);
      return x;
    }
  else
    {
//      fprintf (stderr, "insn uid %d not found\n", uid);
		print(fast_io::c_stderr,"insn uid ",uid," not found\n");
      return 0;
    }
}

/* External entry point for printing a chain of insns
   starting with RTX_FIRST onto file OUTF.
   A blank line separates insns.

   If RTX_FIRST is not an insn, then it alone is printed, with no newline.  */

void
print_rtl (FILE *outf, const_rtx rtx_first)
{
  const_rtx tmp_rtx;
	c_io_lock_guard lg(outf);
  outfile.fp = outf;
  sawclose = 0;

  if (rtx_first == 0)
    {
//      fputs (print_rtx_head, outf);
//      fputs ("(nil)\n", outf);
		print(outfile,print_rtx_head,"(nil)\n");
    }
  else
    switch (GET_CODE (rtx_first))
      {
      case INSN:
      case JUMP_INSN:
      case CALL_INSN:
      case NOTE:
      case CODE_LABEL:
      case BARRIER:
	for (tmp_rtx = rtx_first; tmp_rtx != 0; tmp_rtx = NEXT_INSN (tmp_rtx))
	  {
//	    fputs (print_rtx_head, outfile);
		print(outfile,print_rtx_head);
	    print_rtx (tmp_rtx);
//	    fprintf (outfile, "\n");
		put(outfile,'\n');
	  }
	break;

      default:
//	fputs (print_rtx_head, outfile);
		print(outfile,print_rtx_head);
	print_rtx (rtx_first);
      }
}

/* Like print_rtx, except specify a file.  */
/* Return nonzero if we actually printed anything.  */

int
print_rtl_single (FILE *outf, const_rtx x)
{
	c_io_lock_guard lg(outf);
  outfile.fp = outf;
  sawclose = 0;
/*  fputs (print_rtx_head, outfile);
  print_rtx (x);
  putc ('\n', outf);*/
  print(outfile,print_rtx_head);
  print_rtx (x);
  put(outf,'\n');
  return 1;
}


/* Like print_rtl except without all the detail; for example,
   if RTX is a CONST_INT then print in decimal format.  */

void
print_simple_rtl (FILE *outf, const_rtx x)
{
  flag_simple = 1;
  print_rtl (outf, x);
  flag_simple = 0;
}
